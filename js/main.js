$(function () {
    'use srtict';
    //Adjust slider height
    var windowHeight = $(window).height(),
        navbarHeight = $('.navbar').innerHeight(),
        landingHeight = $('.landing').innerHeight();

    $('.slider').height(windowHeight - (navbarHeight + landingHeight));
});